# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2015-2019 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Instance API
"""

# Imports #####################################################################
from django.apps import apps
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from instance.api.permissions import IsSuperUser
from instance.models.instance import InstanceReference, InstanceTag
from instance.serializers.instance import (
    InstanceReferenceBasicSerializer,
    InstanceReferenceDetailedSerializer,
    InstanceLogSerializer,
    InstanceAppServerSerializer,
    InstanceTagSerializer
)

from .filters import IsOrganizationOwnerFilterBackendInstance, InstanceFilterBackend


# Views - API #################################################################


class InstanceViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API to list and manipulate instances.

    Uses InstanceReference to iterate all types of instances, and serializes them.

    The fields that are returned for each instance depend on its instance_type and whether you
    are listing all instances (returns fewer fields) or just one instance (returns all fields).

    The only fields that are available for all instances, regardless of type, are the fields
    defined on the InstanceReference class, namely:

    * `id`
    * `name`
    * `notes`
    * `created`
    * `modified`
    * `instance_type`
    * `is_archived`

    Note that IDs used for instances are always the ID of the InstanceReference object, which
    may not be the same as the ID of the specific Instance subclass (e.g. the OpenEdXInstance
    object has its own ID which should never be used - just use its InstanceReference ID). This
    detail is managed by the API so users of the API should not generally need to be aware of
    it.
    """
    queryset = InstanceReference.objects.all()
    serializer_class = InstanceReferenceDetailedSerializer
    filter_backends = (IsOrganizationOwnerFilterBackendInstance, InstanceFilterBackend)

    def list(self, request, *args, **kwargs):
        """
        List all instances. No App server list is returned in the list view, only the newest app server information.

        """
        queryset = self.filter_queryset(self.get_queryset())
        if 'include_archived' not in request.query_params:
            # By default, exclude archived instances from the list:
            queryset = queryset.filter(is_archived=False)
        serializer = InstanceReferenceBasicSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def logs(self, request, pk):
        """
        Get this Instance's log entries
        """
        return Response(InstanceLogSerializer(self.get_object()).data)

    @action(detail=True, methods=['get'])
    def app_servers(self, request, pk):
        """
        Get this Instance's entire list of AppServers
        """
        return Response(InstanceAppServerSerializer(self.get_object(), context={'request': request}).data)

    @action(detail=True, methods=['post'])
    def set_notes(self, request, pk):
        """
        Update notes attribute of selected instance.
        """
        if not request.user.is_staff or not request.user.is_superuser:
            return Response(
                {"error": "You do not have permissions to edit this field."},
                status=status.HTTP_403_FORBIDDEN
            )
        if 'notes' not in request.data:
            return Response({'status': 'No notes value provided.'})

        instance = InstanceReferenceBasicSerializer(self.get_object(),
                                                    context={'request': request},
                                                    data={'notes': request.data['notes']},
                                                    partial=True)
        if instance.is_valid():
            instance.save()
            return Response({'status': 'Instance attributes updated.'})
        else:
            return Response(
                {"error": "Instance attributes are not valid."},
                status=status.HTTP_400_BAD_REQUEST
            )

    @action(detail=True, methods=['post'])
    def convert_to_grove_instance(self, request, pk):
        """
        Converts the instance in question to a Grove Instance
        """

        instance = self.get_object()

        grove_instance_class = apps.get_model('grove.GroveInstance')
        grove_instance = grove_instance_class.convert_from_open_edx_instance(instance.instance)

        serializer = self.get_serializer_class()(
            grove_instance.ref,
            context={'request': self.request}
        )

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class InstanceTagViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API to list all *unique* InstanceTag instances.
    """
    queryset = InstanceTag.objects.order_by('name').filter(openedxinstance__isnull=False)
    serializer_class = InstanceTagSerializer
    permission_classes = [IsSuperUser]
