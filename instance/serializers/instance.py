# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2015-2019 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Instance serializers (API representation)
"""

# Imports #####################################################################
from django.apps import apps
from django.db import models

from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from grove.models.instance import GroveInstance
from grove.serializers import GroveInstanceSerializer

from instance.models.instance import InstanceReference, Instance, InstanceTag
from instance.models.openedx_appserver import OpenEdXAppServer
from instance.models.openedx_instance import OpenEdXInstance
from instance.serializers.appserver import AppServerBasicSerializer
from instance.serializers.logentry import LogEntrySerializer
from instance.serializers.openedx_instance import OpenEdXInstanceSerializer


# Serializers #################################################################


class InstanceReferenceMinimalSerializer(serializers.ModelSerializer):
    """
    Serializer for InstanceReference that includes only the ID and URL.
    """
    api_url = serializers.HyperlinkedIdentityField(view_name='api:v1:instance-detail')

    class Meta:
        model = InstanceReference
        fields = (
            'id',
            'api_url',
        )

class InstanceReferenceListSerializer(serializers.ListSerializer):
    """
    ListSerializer for InstanceReference to allow prefetching for AppServers
    """
    # pylint: disable=abstract-method
    def to_representation(self, data):
        """
        Converts a list of InstanceReference objects to their dictionary
        representation.
        """

        # This override exists so that the AppServers can be prefetched for
        # InstanceReference objects. If InstanceReference objects exist
        # with different `instance_type`'s, appservers will not be
        # prefetched and the list will always be empty.

        queryset = data.all() if isinstance(data, models.manager.BaseManager) else data

        appservers_few_columns = OpenEdXAppServer.objects.only('_is_active', '_status', 'id', 'name', 'owner_id',
                                                               'created', 'modified', 'terminated')

        open_edx_content_type = ContentType.objects.get_for_model(apps.get_model('instance.OpenEdXInstance'))
        openedx_instances = [instance for instance in queryset if instance.instance_type_id == open_edx_content_type.pk]

        # Use prefetching to make the number of database queries required to
        # generate this list O(1).
        # Note that prefetching all app servers information is still required, as the "newest" is not decideable
        # at this point. This will cause more data than necessary to be streamed from the DB, but removing this
        # prefetch without first selecting only the "newest" here results in O(n).
        models.prefetch_related_objects(
            openedx_instances,
            models.Prefetch(
                'instance__ref_set__openedxappserver_set',
                queryset=appservers_few_columns,
            ),
            models.Prefetch(
                'instance__ref_set__openedxappserver_set',
                queryset=appservers_few_columns.filter(_is_active=True),
                to_attr='_cached_active_appservers'
            )
        )

        return [
            self.child.to_representation(item) for item in queryset
        ]


class InstanceReferenceBasicSerializer(InstanceReferenceMinimalSerializer):
    """
    Serializer for InstanceReference that includes additional information based on the Instance
    subclass linked to the InstanceReference.

    InstanceReference is a simple class that points to all the concrete 'Instance' subclasses
    such as OpenEdXInstance.
    """

    # summary_only: Uses less detailed serializers for related instances
    summary_only = True

    logs_url = serializers.HyperlinkedIdentityField(view_name='api:v1:instance-logs')
    appservers_full_list_url = serializers.HyperlinkedIdentityField(view_name='api:v1:instance-app-servers')

    class Meta:
        model = InstanceReference
        list_serializer_class = InstanceReferenceListSerializer
        fields = (
            'id',
            'api_url',
            'name',
            'notes',
            'created',
            'modified',
            'is_archived',
            'logs_url',
            'appservers_full_list_url',
        )

    def serialize_details(self, instance):
        """
        Given an object that is a subclass of Instance, serialize it.
        """
        if not isinstance(instance, Instance):
            raise TypeError("InstanceReference.instance should return a subclass of Instance")

        # Use the correct serializer for this type of Instance:
        if isinstance(instance, OpenEdXInstance):
            serializer = OpenEdXInstanceSerializer
        elif isinstance(instance, GroveInstance):
            serializer = GroveInstanceSerializer
        else:
            raise NotImplementedError("No serializer enabled for that Instance type.")

        if self.summary_only and hasattr(serializer, 'basic_serializer'):
            serializer = serializer.basic_serializer

        return serializer(instance, context=self.context).data

    def to_representation(self, instance):
        """
        Add additional fields/data to the output
        """
        output = super().to_representation(instance)
        output['instance_type'] = instance.instance_type.model
        details = self.serialize_details(instance.instance)
        # Merge instance details into the resulting dict, but never overwrite existing fields
        for key, val in details.items():
            output.setdefault(key, val)
        return output


class InstanceReferenceDetailedSerializer(InstanceReferenceBasicSerializer):
    """
    Serializer for InstanceReference that is like InstanceReferenceBasicSerializer but includes
    more detail.
    """
    summary_only = False

    def __init__(self, *args, **kwargs):
        super(InstanceReferenceDetailedSerializer, self).__init__(*args, **kwargs)

        if 'context' in kwargs:
            if 'request' in kwargs['context']:
                request = kwargs['context']['request']
                if not request.user.is_staff or not request.user.is_superuser:
                    self.fields.pop('notes')


class InstanceLogSerializer(serializers.ModelSerializer):
    """
    Provide the log entries for an Instance
    """
    log_entries = LogEntrySerializer(many=True, read_only=True)

    class Meta:
        model = InstanceReference
        fields = ('log_entries', )


class InstanceAppServerSerializer(serializers.ModelSerializer):
    """
    Provide the complete list of app servers for an instance
    """
    class Meta:
        model = InstanceReference
        fields = ('app_servers',)

    def to_representation(self, instance):
        output = super().to_representation(instance)
        output['app_servers'] = [
            AppServerBasicSerializer(appserver, context=self.context).data
            for appserver in instance.app_servers
        ]
        return output


class InstanceTagSerializer(serializers.ModelSerializer):
    """
    Simple high-level serializer for InstanceTag
    """
    class Meta:
        fields = ['id', 'name', 'description']
        model = InstanceTag
