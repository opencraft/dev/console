# -*- coding: utf-8 -*-
#
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2015-2019 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Instance - Integration Tests
"""
# Imports #####################################################################

from datetime import datetime
from unittest.mock import MagicMock

from django.contrib.auth import get_user_model

from instance.models.appserver import AppServer
from instance.models.openedx_instance import OpenEdXInstance
from instance.tests.integration.base import IntegrationTestCase
from instance.tests.integration.factories.instance import OpenEdXInstanceFactory
from registration.approval import on_appserver_spawned
from registration.models import BetaTestApplication
from userprofile.models import UserProfile


# Tests #######################################################################


class InstanceIntegrationTestCase(IntegrationTestCase):
    """
    Integration test cases for instance high-level tasks
    """

    def test_betatest_accepted(self):
        """
        Provision an instance, spawn an AppServer and accepts the application.
        """
        OpenEdXInstanceFactory(
            name='Integration - test_betatest_accepted',
            deploy_simpletheme=True,
        )
        instance = OpenEdXInstance.objects.get()

        # Add an lms user, as happens with beta registration
        user, _ = get_user_model().objects.get_or_create(username='test', email='test@example.com')
        instance.lms_users.add(user)

        # Create user profile and update user model from db
        UserProfile.objects.create(
            user=user,
            full_name="Test user 1",
            accepted_privacy_policy=datetime.now(),
            accept_domain_condition=True,
            subscribe_to_updates=True,
        )
        user.refresh_from_db()

        # Simulate that the application form was filled. This doesn't create another instance nor user
        BetaTestApplication.objects.create(
            user=user,
            subdomain='betatestdomain',
            instance_name=instance.name,
            public_contact_email='publicemail@example.com',
            project_description='I want to beta test OpenCraft IM',
            status=BetaTestApplication.PENDING,
            instance=instance,
        )

        appserver = MagicMock()
        appserver.status = AppServer.Status.Running
        instance.refresh_from_db()

        # Test accepting beta test application
        on_appserver_spawned(None, instance=instance, appserver=appserver)
        self.assertEqual(instance.betatestapplication.first().status, BetaTestApplication.ACCEPTED)
